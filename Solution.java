import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;

class Solution {
    public int solution(int N) {
        
        String binary = Integer.toBinaryString(N);
        String regex = "1";
        Matcher matcher = Pattern.compile(regex).matcher(binary);
        List<Integer> matches = new ArrayList();
        int gap = 0;
        
        while(matcher.find()) {
            matches.add(matcher.start());
        }
        
        if(matches.size() > 1) {
            for(int i = 1; i < matches.size(); i++) {
                int currentGap = matches.get(i) - matches.get(i-1) - 1;
                if (currentGap > gap)
                    gap = currentGap;
            }
        } else {
            return 0;
        }
        
        return gap;
    }
}